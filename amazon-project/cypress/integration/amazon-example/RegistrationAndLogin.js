describe('amazon site', ()=>{
    
    beforeEach('Open amazon.com url',()=>{

        cy.visit("https://www.amazon.com/")
        .url().should('include','amazon')
        .wait(5000)
    })

    it('New User Registration',()=>{

        cy.get('.nav-line-2').contains('Account & Lists').click()
        .get('#createAccountSubmit').click()
        .get('#ap_customer_name').type('Radhey Shyam')
        .get('#ap_email').type('radhey3910@gmail.com')
        .get('#ap_password').type('shyamradhey')
        .get('#ap_password_check').type('shyamradhey')
        .get('.a-button-input').click()
    })
    it('Sign-In to amazon ',()=>{
        
        cy.get('#nav-link-accountList-nav-line-1').click()
        cy.get('#ap_email').type('radhey3910@gmail.com')
        cy.get('.a-button-input[type="submit"]').click()

        cy.get('#ap_password').type('shyamradhey')
        cy.get('.a-button-input[type="submit"]').click()
    })
})