describe('amazon site', ()=>{
    
    before('Open amazon.com url',()=>{
        
        cy.visit("https://www.amazon.com/")
        .url().should('include','amazon')
        .wait(5000)
    })

    it('Add product to cart',()=>{
        cy.get('#twotabsearchtextbox').type('SAMSUNG 15.6” Galaxy Book2 Pro Laptop Computer')
        cy.get('#nav-search-submit-button').click({force:true})
        cy.wait(5000)

        cy.log('Product Searching')
    
        cy.get('.s-title-instructions-style > h2 > a > span')
        cy.contains('SAMSUNG 15.6” Galaxy Book2 Pro Laptop Computer, i7 / 16GB / 512GB, 12th Gen Intel Core Processor, Evo Certified, Lightweight, 2022 Model, Graphite').click()
        cy.wait(5000)
        
        cy.log('Product are Selected')

        cy.get('.a-dropdown-label').click()
        .get('.a-dropdown-item').contains('2').click()

        .get('#add-to-cart-button').click()
        .wait(5000)
        .get('.a-button-input[aria-labelledby="attach-sidesheet-view-cart-button-announce"]').click({force:true}) 
    })
})