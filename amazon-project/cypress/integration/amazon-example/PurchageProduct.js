describe('amazon site', ()=>{
    
    before('Open amazon.com url',()=>{
      
        cy.visit("https://www.amazon.com/")
        .url().should('include','amazon')

    })
  
    it('Purchage product from amazon ',()=>{
        
        cy.log('Start SignIn to amazon')

        cy.get('#nav-link-accountList-nav-line-1').click()

        cy.get('#ap_email').type('radhey3910@gmail.com')
        cy.get('.a-button-input[type="submit"]').click()

        cy.get('#ap_password').type('shyamradhey')
        cy.get('.a-button-input[type="submit"]').click()

        cy.log('Successfully Sign In')

        cy.get('#twotabsearchtextbox').type('SAMSUNG 15.6” Galaxy Book2 Pro Laptop Computer')
        cy.get('#nav-search-submit-button').click()
        cy.wait(5000)

        cy.log('Product Searching')
    
        cy.get('.s-title-instructions-style > h2 > a > span')
        cy.contains('SAMSUNG 15.6” Galaxy Book2 Pro Laptop Computer, i7 / 16GB / 512GB, 12th Gen Intel Core Processor, Evo Certified, Lightweight, 2022 Model, Graphite')
        .click({force:true})
        cy.wait(5000)
        
        cy.log('Product are Selected')

        cy.get('.a-dropdown-label').contains('Qty:').click()
        cy.get('.a-dropdown-item').contains('2').click()
        
        cy.log('Choose quantity of Product')

        cy.get('#buy-now-button').click()

        cy.log('Finaly purchage the product')
            
    })

})