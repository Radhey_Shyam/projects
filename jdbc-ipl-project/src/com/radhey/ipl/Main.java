package com.radhey.ipl;

import java.sql.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        String jdbcURL = "jdbc:postgresql://localhost:5432/ipl_project";
        String username = "radhey";
        String password = "shyam";
        try {
            Connection connection = DriverManager.getConnection(jdbcURL, username, password);
            System.out.println("connected to PostgreSQL server");

            String sql = "SELECT * FROM match_table";
            Statement statment = connection.createStatement();
            ResultSet result = statment.executeQuery(sql);
            List<Match> matchList = new ArrayList<>();


            while (result.next()) {
                Match match = new Match();
                match.setMatchId(result.getInt("match_id"));
                match.setSession(result.getInt("season"));
                match.setWinnerTeam(result.getString("winner"));
                if (result.wasNull()) {
                    match.setWinnerTeam("");
                }
                matchList.add(match);
            }
            String query = "SELECT * FROM delivery";
            Statement deliveryStatement = connection.createStatement();
            ResultSet result1 = deliveryStatement.executeQuery(query);
            List<Delivery> deliveryList = new ArrayList<>();
            while (result1.next()) {
                Delivery delivery = new Delivery();
                delivery.setMatchId(result1.getInt("delivery_id"));
                delivery.setBowler(result1.getString("bowler"));
                delivery.setWideRun(result1.getInt("wide_runs"));
                delivery.setTotalRun(result1.getInt("total_runs"));
                delivery.setBattingTeam(result1.getString("batting_team"));
                delivery.setExtraRun(result1.getInt("extra_runs"));
                deliveryList.add(delivery);
            }

            numberOfMatchesPlayedEveryYear(matchList);
            totalMatchesOwnByEachTeam(matchList);
            extraRunMadeByEachTeamIn2016(matchList, deliveryList);
            topEconomicalBowlerOf2015(matchList, deliveryList);

            connection.close();
        } catch (SQLException e) {
            System.out.println("Error in connecting to PostgreSQL server");
            e.printStackTrace();
        }
    }
    public static void numberOfMatchesPlayedEveryYear(List<Match> matchList) {

        TreeMap<Integer, Integer> teamByYear = new TreeMap<>();
        for (Match match : matchList) {
            if (teamByYear.containsKey(match.getSession())) {
                teamByYear.put(match.getSession(), teamByYear.get(match.getSession()) + 1);
            } else {
                teamByYear.put(match.getSession(), 1);
            }
        }
        System.out.println("\n1. Number of matches played per year of all the years in IPL.");
        for (Map.Entry<Integer, Integer> entry : teamByYear.entrySet()) {
            System.out.println(entry.getKey() + " = " + entry.getValue());
        }
    }

    public static void totalMatchesOwnByEachTeam(List<Match> matchList) {

        TreeMap<String, Integer> wonMatchesByTeam = new TreeMap<>();
        for (Match match : matchList) {
            if (wonMatchesByTeam.containsKey(match.getWinnerTeam())) {
                wonMatchesByTeam.put(match.getWinnerTeam(), wonMatchesByTeam.get(match.getWinnerTeam()) + 1);
            } else if (!match.getWinnerTeam().equals("")) {
                wonMatchesByTeam.put(match.getWinnerTeam(), 1);
            }
            //System.out.println(match.getWinnerTeam());
        }
        System.out.println("\n2. Number of matches won of all teams over all the years of IPL.");
        for (Map.Entry<String, Integer> entry : wonMatchesByTeam.entrySet())
            System.out.println(entry.getKey() + " = " + entry.getValue());
    }

    public static void extraRunMadeByEachTeamIn2016(List<Match> matchList, List<Delivery> deliveryList) {
        ArrayList<Integer> IdOf2016Matches = new ArrayList<>();
        for (Match match : matchList) {
            if (match.getSession() == 2016) {
                IdOf2016Matches.add(match.getMatchId());
            }
        }
        TreeMap<String, Integer> extraRunByTeam = new TreeMap<>();
        for (Integer id : IdOf2016Matches) {
            for (Delivery delivery : deliveryList) {

                if (id == delivery.getMatchId()) {
                    if (extraRunByTeam.containsKey(delivery.getBattingTeam())) {

                        extraRunByTeam.put(delivery.getBattingTeam(), extraRunByTeam.get(delivery.getBattingTeam()) +
                                delivery.getExtraRun());
                    } else {
                        extraRunByTeam.put(delivery.getBattingTeam(), delivery.getExtraRun());
                    }
                }
            }
        }
        System.out.println("\n3. For the year 2016 get the extra runs conceded per team.");
        for (Map.Entry<String, Integer> entry : extraRunByTeam.entrySet())
            System.out.println(entry.getKey() + " = " + entry.getValue());
    }

    public static void topEconomicalBowlerOf2015(List<Match> matchList, List<Delivery> deliveryList) {

        ArrayList<Integer> idOf2015Matches = new ArrayList<>();
        for (Match match : matchList) {
            if (match.getSession() == 2015) {
                idOf2015Matches.add(match.getMatchId());
            }
        }
        HashMap<String, ArrayList<Integer>> totalRunAndBallByBowlerName = new HashMap<>();

        for (Integer id : idOf2015Matches) {
            for (Delivery delivery : deliveryList) {

                if (delivery.getMatchId() == id) {
                    if (totalRunAndBallByBowlerName.containsKey(delivery.getBowler())) {
                        ArrayList<Integer> list = totalRunAndBallByBowlerName.get(delivery.getBowler());
                        list.set(0, list.get(0) + delivery.getTotalRun());
                        list.set(1, list.get(1) + 1);
                        if (delivery.getWideRun() != 0) {   // if wide ball then decrease ball number
                            list.set(1, list.get(1) - 1);
                        }
                        totalRunAndBallByBowlerName.put(delivery.getBowler(), list);

                    } else {
                        ArrayList<Integer> empityList = new ArrayList<>();
                        empityList.add(0, delivery.getTotalRun());
                        empityList.add(1, 1);
                        totalRunAndBallByBowlerName.put(delivery.getBowler(), empityList);
                    }
                }
            }
        }
        double minValue = Integer.MAX_VALUE;
        String bowlerName = "";
        for (Map.Entry<String, ArrayList<Integer>> entry : totalRunAndBallByBowlerName.entrySet()) {
            ArrayList<Integer> list = totalRunAndBallByBowlerName.get(entry.getKey());
            double score = list.get(0);
            double ball = list.get(1);
            int BALL_IN_A_OVER = 6;
            double economyValue = (score / ball) * BALL_IN_A_OVER;
            if (economyValue < minValue) {
                minValue = economyValue;
                bowlerName = entry.getKey();
            }
        }
        System.out.println("\n4. Top economical bowler of 2015.");
        System.out.printf(bowlerName + " : " + "%.2f\n", minValue);
    }
}
