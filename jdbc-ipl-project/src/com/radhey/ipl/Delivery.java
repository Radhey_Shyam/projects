package com.radhey.ipl;

public class Delivery {
    private int matchId;
    private String bowler;
    private int wideRun;
    private int totalRun;
    private  String battingTeam;
    private  int extraRun;


    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public String getBowler() {
        return bowler;
    }

    public void setBowler(String bowler) {
        this.bowler = bowler;
    }

    public int getWideRun() {
        return wideRun;
    }

    public void setWideRun(int wideRun) {
        this.wideRun = wideRun;
    }

    public int getTotalRun() {
        return totalRun;
    }

    public void setTotalRun(int total_Run) {
        this.totalRun = total_Run;
    }


    public String getBattingTeam() {
        return battingTeam;
    }

    public void setBattingTeam(String battingTeam) {
        this.battingTeam = battingTeam;
    }

    public int getExtraRun() {
        return extraRun;
    }

    public void setExtraRun(int extraRun) {
        this.extraRun = extraRun;
    }
}
